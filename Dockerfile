FROM python:3.7.5-buster

WORKDIR /usr/src/app

ENV PYTHONUNBUFFERED 1
ENV PATH="/usr/src/app:${PATH}"

# Install python dependencies
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

# CMD [ "python", "-u", "./get_data.py" ]
CMD [ "scrapy", "crawl", "inpres" ]
