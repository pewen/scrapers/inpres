# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


import os
import json
import hashlib
from shutil import copyfile
from datetime import datetime

import pandas as pd
from scrapy.exporters import CsvItemExporter


def fix_seg(date):
    # Algunos fechas tienen mal los segundos (> 59)
    # Si es asi, solo lo mando a 59
    parts = date.split(':')
    seg = int(parts[-1])

    if seg > 60:
        seg = '59'

    return ':'.join(parts[:-1] + [str(seg)])


class ToCsvPipeline(object):
    @classmethod
    def from_crawler(cls, crawler):
        settings = crawler.settings

        return cls(
            spider_name=crawler.spider.name,
            header=settings.get('HEADER'),
            sort_by=settings.get('SORT_BY'),
            output_dir=settings.get('OUTPUT_DIR'),
            proyect=settings.get('BOT_NAME')
        )

    def __init__(self, spider_name, header, sort_by, output_dir, proyect):
        self.sort_by = sort_by
        self.output_file_path = f'{output_dir}/{proyect}/{spider_name}.csv'

        if not os.path.isdir(output_dir):
            os.mkdir(output_dir)

        if not os.path.isdir(f'{output_dir}/{proyect}'):
            os.mkdir(f'{output_dir}/{proyect}')

        self.file = open(self.output_file_path, 'ab')
        if os.stat(self.output_file_path).st_size == 0:
            self.file.write(str.encode(','.join(header) + '\n'))

        self.exporter = CsvItemExporter(self.file, include_headers_line=False)
        self.exporter.fields_to_export = header
        self.exporter.start_exporting()

    def close_spider(self, spider):
        self.exporter.finish_exporting()
        self.file.close()

    def process_item(self, item, spider):
        self.exporter.export_item(item)
        return item


class ClearCsvPipeline(object):
    @classmethod
    def from_crawler(cls, crawler):
        settings = crawler.settings

        return cls(
            spider_name=crawler.spider.name,
            sort_by=settings.get('SORT_BY'),
            output_dir=settings.get('OUTPUT_DIR'),
            proyect=settings.get('BOT_NAME')
        )

    def __init__(self, spider_name, sort_by, output_dir, proyect):
        self.sort_by = sort_by
        self.output_file_path = f'{output_dir}/{proyect}/{spider_name}.csv'

    def close_spider(self, spider):
        # Sort CSV
        df = pd.read_csv(self.output_file_path)
        df.fecha = df.fecha.apply(fix_seg)
        df.fecha = pd.to_datetime(df.fecha)
        df.sort_values(by=self.sort_by)

        # Fix some names
        df.provincia = df.provincia.fillna('')
        df.provincia = df.provincia.apply(lambda ele: ' '.join(ele.upper().split()))
        df.provincia = df.provincia.str.replace('NEUQUEN', 'NEUQUÉN')

        df.to_csv(self.output_file_path, index=False)

    def process_item(self, item, spider):
        return item


class DataPackagedPipeline(object):
    @classmethod
    def from_crawler(cls, crawler):
        settings = crawler.settings

        return cls(
            spider_name=crawler.spider.name,
            output_dir=settings.get('OUTPUT_DIR'),
            proyect=settings.get('BOT_NAME')
        )

    def __init__(self, spider_name, output_dir, proyect):
        self.spider_name = spider_name
        self.datapackage_path = f'{output_dir}/{proyect}/datapackage.json'
        self.output_file_path = f'{output_dir}/{proyect}/{spider_name}.csv'

        if not os.path.isfile(self.datapackage_path):
            copyfile('./datapackage.json', self.datapackage_path)

    def close_spider(self, spider):
        name = f'smn_{self.spider_name}'

        with open(self.datapackage_path) as file:
            datapackage = json.load(file)

        for i, resource in enumerate(datapackage['resources']):
            if resource['name'] == name:
                break

        md5sum = hashlib.md5(open(self.output_file_path,'r').read().encode()).hexdigest()

        datapackage['resources'][i]['hash'] = md5sum
        datapackage['resources'][i]['bytes'] = os.path.getsize(self.output_file_path)
        datapackage['resources'][i]['last_updated'] = datetime.now().isoformat()

        with open(self.datapackage_path, 'w') as file:
            json.dump(datapackage, file, indent=2, ensure_ascii=False)

    def process_item(self, item, spider):
        return item
