import os
# import json
# import hashlib
# from shutil import copyfile
from datetime import datetime

import scrapy
import pandas as pd
from bs4 import BeautifulSoup
from scrapy.http import FormRequest

from inpres.items import InpresItem


def sort_date(date, hour):
    d, m, y = date.split('/')
    return f'{y}-{m}-{d}T{hour}'


class InpresSpider(scrapy.Spider):
    name = 'inpres'

    custom_settings = {
        'HEADER': ['fecha', 'latitud', 'longitud', 'profundidad',
                   'magnitud', 'intensidad', 'provincia', 'url'],
        'SORT_BY': ['fecha']
    }

    def start_requests(self):
        # Primer registro existente
        first_register = '29/07/1998 04:14:25'

        proyect = self.settings.get('BOT_NAME')
        output_dir = self.settings.get('OUTPUT_DIR')
        output_file_path = f'{output_dir}/{proyect}/{self.name}.csv'

        # Set datetime limit base on the last scraped
        if os.stat(output_file_path).st_size == 0:
            self.from_date = first_register.split()[0]
            self.to_date = datetime.strftime(datetime.now(), '%d/%m/%Y')
        else:
            self.df = pd.read_csv(output_file_path)
            self.date_min = pd.to_datetime(self.df.fecha).min()
            self.date_max = pd.to_datetime(self.df.fecha).max()

            if self.date_min == datetime.strptime(first_register, '%d/%m/%Y %H:%M:%S'):
                # Ya scrapie todo lo viejo
                self.from_date = datetime.strftime(self.date_max, '%d/%m/%Y')
                self.to_date = datetime.strftime(datetime.now(), '%d/%m/%Y')
            else:
                self.from_date = first_register.split()[0]
                self.to_date = datetime.strftime(self.date_min, '%d/%m/%Y')

        self.logger.info(f'Scrapenado desde la fecha {self.from_date} al {self.to_date}')

        url = 'http://contenidos.inpres.gob.ar/buscar_sismo'
        yield scrapy.Request(url=url, callback=self.fill_form)

    def fill_form(self, response):
        data = {
            'datepicker': self.from_date,
            'datepicker2': self.to_date,
            'tilde1': 'checkbox'
        }

        return FormRequest.from_response(response, callback=self.parse,
            formdata=data)

    def parse(self, response):
        # Fint the table
        soup = BeautifulSoup(response.body, 'html.parser')
        table = soup.find('table', {'id': 'sismos'})

        # Parse the table
        rows = table.find_all('tr')[2:]

        for row in rows:
            partial = row.find_all('td')
            line = [ele.text for ele in partial[1:-1]] + [partial[-1].find('a').attrs['href']]

            item = InpresItem()

            item['fecha'] = sort_date(line[0], line[1])
            item['latitud'] = line[2]
            item['longitud'] = line[3]
            item['profundidad'] = line[4].split()[0]
            item['magnitud'] = line[5]
            item['intensidad'] = line[6]
            item['provincia'] = line[7]
            item['url'] = 'http://contenidos.inpres.gob.ar/' + line[8]

            yield item

        # Next table
        is_more = soup.select('td.Estilo68 > font > a:nth-last-child(1)')
        if is_more:
            link = is_more[0].attrs['href']
            url = 'http://contenidos.inpres.gob.ar/' + link
            url = ''.join(url.split())
            yield scrapy.Request(url=url, callback=self.parse)
