# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class InpresItem(scrapy.Item):
    fecha = scrapy.Field()
    latitud = scrapy.Field()
    longitud = scrapy.Field()
    profundidad = scrapy.Field()
    magnitud = scrapy.Field()
    intensidad = scrapy.Field()
    provincia = scrapy.Field()
    url = scrapy.Field()
