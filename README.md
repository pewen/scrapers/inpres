# INPRES scraper

Scraper de los sismos relevados por el
[INPRES](https://www.inpres.gob.ar/desktop/) (Instituto Nacional de prevención sísmica).  

Los datos estan [acá](https://gitlab.com/pewen/scrapers/inpres/-/tree/master/data/inpres).

## Columnas

| Columna     | Tipo   | Unidad | Descripcion                               |
| :---        | :---:  | :---:  | ---:                                      |
| fecha       | string |        | Fecha del relevamiento del dato dd/mm/yyy |
| hora        | int    |        | Hora del relevamiento                     |
| latitud     | int    |        | Latitud del sismo                         |
| longitud    | int    |        | Longitud del sismo                        |
| profundidad | float  | Km     | Profundidad del sismo                     |
| magnitud    | float  |        |                                           |
| intensidad  | float  |        |                                           |
| provincia   | string |        |                                           |
| mapa        | string |        |                                           |

## Docker

```bash
$ docker-compose build
$ docker-compose up
```

# Por que el scraper?

* No se puede descargar todos los datos en un solo archivo.
* Sitio complicado de procesar
